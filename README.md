# Ubuntu Touch device tree for Planet Gemini PDA

This repository contains the build setup and device-specific overlay for the Gemini PDA to run Ubuntu Touch. It uses Halium 9.0 with custom Treblized vendor.img and the mechanism described in [this page](https://github.com/ubports/porting-notes/wiki/GitLab-CI-builds-for-devices-based-on-halium_arm64-(Halium-9)).

This project can be built manually (see the instructions below) or you can download the ready-made artifacts from GitLab: take the latest [devel-flashable](https://gitlab.com/ubports/porting/community-ports/android9/planet-geminipda/planet-geminipda/-/jobs/artifacts/master/download?job=devel-flashable) or [devel-flashable-focal archive](https://gitlab.com/ubports/porting/community-ports/android9/planet-geminipda/planet-geminipda/-/jobs/artifacts/master/download?job=devel-flashable-focal) (for Ubuntu Touch based on Ubuntu 20.04), unpack the `artifacts.zip` file (make sure that all files are created inside a directory called `out/`, then follow the instructions in the [Installation](#Installation) section.

## Installation

1. Download the devel-flashable or devel-flashable-focal (for Ubuntu Touch 20.04) job's artifacts.zip from the CI/CD pipeline of the project.
2. Unpack the out/ubuntu.img file from the artifacts and put the ubuntu.img file at the root of the Linux (Gemini) partition. If there is ubuntu.img.zstd instead, decompress it first.
3. Flash the out/boot.img to one of the available boot slots. For example, use the following command to flash it to boot3 slot:
```bash
cat boot.img > /dev/disk/by-partlabel/boot3
```

## How to build
To manually build this project, follow these steps:

```bash
./build.sh -b workdir  # workdir is the name of the build directory
./build/prepare-fake-ota.sh out/device_Gemini_4G.tar.xz ota
./build/system-image-from-ota.sh ota/ubuntu_command out
```
